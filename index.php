<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>tvujdomek</title>
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
	
		<script src="/js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="/js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
		<script src="/js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
	</head>
    
 <a href="" id="arrow_left"><img src="/img/demo/arrow_left.png" alt="Slide Left" /></a>
 <a href="" id="arrow_right"><img src="/img/demo/arrow_right.png" alt="Slide Right" /></a>
	<body id="body" class="brChrome">
		<div class="header">PRODEJ MOBILNÍCH DOMŮ  20 - 150 m<sup><small>2</small></sup></div>
		<div class="content" id="content">
			<div id="maximage">
				<img src="/img/slider/main2.png" alt="" />
                <img src="/img/slider/Mini_21.png" alt="" />
                <img src="/img/slider/Prima_72.png" alt="" />
			</div>
		</div>
		<div class="footer">
			<div class="address">
				<p>Prohlídka po domluvě: 39701 Písek</p>
				<p>Městský areál, ul. Sedláčková 6</p>
			</div>
			<div class="tel">tel. 774 041 105</div>
			<div class="email">tvujdomek@gmail.com</div>
		</div>
	<!--	<div class="red_icon"></div>-->
		
		<!--<script type="text/javascript" charset="utf-8">
			$(function(){
				jQuery('#maximage').maximage({
					fillElement: '#content',
					backgroundSize: 'contain'
				});
			});
		</script>-->
        
        <script type="text/javascript" charset="utf-8">
        	$(function(){
        		// Trigger maximage
        		jQuery('#maximage').maximage({
        		  	fillElement: '#content',
					backgroundSize: 'contain',
        		cycleOptions: {
        			fx: 'fade',
        			//speed: 1000,
        			timeout: 5000,
        			prev: '#arrow_left',
        			next: '#arrow_right',
        			}
        		});
        	});
        </script>
	</body>
</html>